import { Component, OnInit } from '@angular/core';

const CLIENT_ID = 'sq0idp-KRlkzX5inLy6wICUDnvtPg';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  appIntentHref: string = '';

  ngOnInit() {
    if (this.getMobileOperatingSystem() === 'ios') {
      this.appIntentHref =  this.buildIosHref();
    }

    this.appIntentHref =  this.buildAndroidHref();
  }

  getMobileOperatingSystem() {
    const userAgent =
      navigator.userAgent || navigator.vendor || (window as any).opera;

    if (/android/i.test(userAgent)) {
      return 'android';
    }

    if (/iPad|iPhone|iPod/.test(userAgent) && !(window as any).MSStream) {
      return 'ios';
    }

    return 'unknown';
  }


  buildIosHref() {
    const iosSquareData = {
      amount_money: {
        amount: 100,
        currency_code: 'GBP'
      },
      callback_url: 'https://test.givetap.co.uk',
      client_id: CLIENT_ID,
      version: '1.3',
      options: {
        supported_tender_types: ['CREDIT_CARD']
      },
      state: '12345',
      notes: 'GiveTap::12345'
    };

    const appDataStringified = JSON.stringify(iosSquareData);
    const appDataEncodedUri = encodeURIComponent(appDataStringified);

    return `square-commerce-v1://payment/create?data=${appDataEncodedUri}`;
  }

  buildAndroidHref() {
    return `
        intent:#Intent;
        action=com.squareup.pos.action.CHARGE;
        package=com.squareup;
        S.browser_fallback_url=https://test.givetap.co.uk;
        S.com.squareup.pos.WEB_CALLBACK_URI=https://test.givetap.co.uk;
        S.com.squareup.pos.CLIENT_ID=${CLIENT_ID};
        S.com.squareup.pos.API_VERSION=v2.0;
        i.com.squareup.pos.TOTAL_AMOUNT=100;
        S.com.squareup.pos.CURRENCY_CODE=GBP;
        S.com.squareup.pos.TENDER_TYPES=com.squareup.pos.TENDER_CARD;
        S.com.squareup.pos.REQUEST_METADATA="12345";
        S.com.squareup.pos.NOTE=GiveTap::12345;
        end
    `
  }
}
